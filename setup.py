from setuptools import setup
from surelite import __version__

setup(
    name="surelite",
    description="Python API for the Continuum Surelite laser",
    version=__version__,
    platforms=["any"],
    author="Markus J. Schmidt",
    author_email='schmidt@ifd.mavt.ethz.ch',
    license="GNU GPLv3+",
    url="https://gitlab.ethz.ch/ifd-lab/device-driver/pysurelite",
    py_modules=["surelite"],
    install_requires=['pyserial>=3.4'],
    classifiers=[
        'Development Status :: 4 - Beta',
        'Intended Audience :: Science/Research',
        'Topic :: Scientific/Engineering',
        'License :: OSI Approved :: GNU General Public License v3 '
        'or later (GPLv3+)',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7'
    ],
    zip_safe=False
)
