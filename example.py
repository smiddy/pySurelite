import surelite
import logging

port = 'COM5'

# Create logger
logging.basicConfig(level=logging.DEBUG, filename='example.log',
                    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')

with surelite.Device(port, 'A') as laserA:
    counter = laserA.sendrcv('SC')
    security = laserA.sendrcv('SE')
    err = laserA.sendrcv('SP 1')
pass

# with serial.Serial('COM5', baudrate=9600, bytesize=8, parity='N', stopbits=1, xonxoff=0, rtscts=0,
#                    timeout=1) as ser:
#     ser.write(b'SE\r')
#     interlocks = ser.readlines()
#     ser.write(b'SC\r')
#     counter = ser.readlines()
#     pass
