# pySurelite

Python API for the Continuum Surelite

Tested with
* Win 10 64bit
* Python 3.7.1 (Anaconda)
* pyserial 3.4

## Installation
Installation from project folder in current Python environment with

```python
python setup.py install
```

## Classes
The class surelite.Device is the main class for interaction. Commands can be executed
with the `sendrcv` method. If command is a query, the function returns the answer as integer.

The class `surelite.SureliteError` issues errors.

A context manager is availabe for safe operation. Please check `example.py` for an example.
 