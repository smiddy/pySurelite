import serial
import logging
from time import sleep
import io

# Set version
__version__ = '0.1.0-beta2'

# create logger
logger = logging.getLogger(__name__)


class SureliteError(Exception):
    """
    PHD2000 exception handle
    """

    def __init__(self, err: int, command: str = None):
        self.logger = logging.getLogger(__name__)
        if err == -1:
            self.errStr = 'Unknown command. Was "{}"'.format(command)
            self.logger.error(self.errStr)
            return
        else:
            self.errdict = {-1: 'No response from device.',
                            1: 'Surelite not in serial mode',
                            2: 'Coolant flow interrupted',
                            3: 'Coolant temperature over temp',
                            4: None,  # not used, see manual p.3-18
                            5: 'Laser head problem',
                            6: 'External interlock',
                            7: 'End of charge not detected before lamp fire',
                            8: 'Simmer not detected',
                            9: 'Flow switch stuck on'}
            self.errStr = self.errdict[err]
            self.logger.error(self.errStr)
            return

    def __str__(self):
        return self.errStr


class Device(object):
    def __init__(self, comport: str, label: str = 'A'):
        """ Device class for Continuum Surelite Laser

        :param comport: address of serial port
        :param rate: baudrate
        :param label: Label for laser identification. Default A
        """
        self.__version__ = __version__
        self.logger = logging.getLogger(__name__)
        self.comlink = serial.Serial(comport, baudrate=9600, bytesize=8, parity='N', stopbits=1, xonxoff=0, rtscts=0,
                                     timeout=1)
        self.label = label
        self._commands = ['SS', 'SE', 'SH', 'ST', 'QS', 'VA', 'PD', 'SC']
        if not self.comlink.isOpen():
            self.comlink.open()
        # Empty the read cache
        _ = self.comlink.read(self.comlink.in_waiting)
        # Check for errors
        self.sendrcv('SE')
        self.logger.debug("Laser {} successful initialized".format(self.label))

    def sendrcv(self, command):
        """Takes a command and send it

        The string is converted to a compatible byte and send
        to the delay generator. The answer is analysed and in
        case a SureliteError is raised.

        :param command: Command string
        :type: string
        :returns: output (if appropriate)
        :rtype: int
        """
        sendbyte = bytearray(command, 'ASCII') + b'\r'
        self._errcheck(command)
        if not self.comlink.isOpen():
            self.comlink.open()
        self.comlink.write(sendbyte)
        while not (self.comlink.out_waiting == 0):
            sleep(0.1)
        response = self.comlink.read_until(b'\r')
        if not response:
            raise SureliteError(-1, command)
        if command in ['SE', 'SC']:
            output = self.comlink.read_until(b'\r')
            if command == 'SE' and int(output[:-1].decode('ASCII')) != 0:
                raise SureliteError(int(output[:-1].decode('ASCII')), command)
            self.logger.debug('Command "{}" issued'.format(command))
            return int(output[:-1].decode('ASCII'))
        else:
            self.logger.debug('Command "{}" issued'.format(command))
            return

    def _errcheck(self, command: str):
        validcmd = [command.startswith(cmd) for cmd in self._commands]
        if not any(validcmd):
            raise SureliteError(-1, command)

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        if self.comlink.isOpen():
            self.sendrcv('ST 0')
            self.sendrcv('SH 0')
            self.comlink.close()
